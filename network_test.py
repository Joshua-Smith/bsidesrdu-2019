import struct

from plugins.compression import zlib
from plugins.cryptography import AES256CBC
from plugins.encoding import base64
from plugins.steganography import markov
import plugins.net.udp.sip as network
from engine import Engine
from plugins.misc import swap

aes = AES256CBC("asdfawefa")

ENCODER = [
    zlib,
    aes,
    swap,
    base64,
    markov
]

z = Engine(ENCODER)


def recv_data(data):
    y = z.decode(data)


network.listen(8787, recv_data)


