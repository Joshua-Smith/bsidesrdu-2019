from plugins.net import detWrapper
import plugins.modules.det.plugins.tcp as detTCP

det = detWrapper(None)

def send(target, port, data):
    config = {
        "target": target,
        "port": port
    }
    detTCP.Plugin(det,config)
    detTCP.send(data)

def listen(port,func):
    det.cb = func
    config = {
        "port": port
    }
    detTCP.Plugin(det,config)
    detTCP.listen()