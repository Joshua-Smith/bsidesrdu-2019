class detWrapper(object):
    def __init__(self, cb):
        self.cb = cb

    def retrieve_data(self, data, *kw, **kwargs):
        self.cb(data)

    def log_message(self, *kw, **kwargs):
        print(kw)
        print(kwargs)

    def register_plugin(self, *kw, **kwargs):
        pass