import plugins.modules.pyexfil.network.DNS.dns_exfil as pyexfilDNS

def send(target, data, port=53):
    pyexfilDNS.dns_exfil(target ,data, port)

def listen(port):
    pyexfilDNS.dns_server(port=port)