from plugins.net import detWrapper
import plugins.modules.det.plugins.udp as detUDP

det = detWrapper(None)

def send(target, port, data):
    config = {
        "target": target,
        "port": port
    }
    detUDP.Plugin(det,config)
    detUDP.send(data)

def listen(port,func):
    det.cb = func
    config = {
        "port": port
    }
    detUDP.Plugin(det,config)
    detUDP.listen()