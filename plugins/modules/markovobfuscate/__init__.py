from .formatters import LyricsObfuscator
import zlib

TRAINING_FILE = "ts_lyrics.lst"

def encode(data):

    obfuscator = LyricsObfuscator
    m1 = obfuscator(64)
    with open(TRAINING_FILE, "r") as f:
        text = f.read()
    m1.learn_book(text)
    return m1.obfuscate_string(data)


def decode(data):
    obfuscator = LyricsObfuscator
    m1 = obfuscator(64)
    m2 = obfuscator(64)
    with open(TRAINING_FILE, "r") as f:
        text = f.read()
    m1.learn_book(text)
    m2.learn_book(text)
    r = m1.deobfuscate_string(data)
    return r
