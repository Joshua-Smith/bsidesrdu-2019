import zlib

def encode(data):
    return zlib.compress(data, 9)

def decode(data):
    return zlib.decompress(data)