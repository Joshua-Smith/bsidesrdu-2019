import hashlib
from Crypto.Cipher import AES
from Crypto import Random


class AES256CBC(object):
    def __init__(self, key):
        self.__name__ = "AES256CBC"
        self.bs = AES.block_size
        self._key = hashlib.sha256(key.encode()).digest()

    @property
    def key(self):
        return self._key

    def encode(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(raw)

    def decode(self, enc):
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:]))

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs).encode()

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]