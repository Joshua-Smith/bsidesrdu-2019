import struct

from plugins.compression import zlib
from plugins.cryptography import AES256CBC
from plugins.encoding import base64, yenc
from plugins.steganography import markov
import plugins.net.udp.dns as network
from engine import Engine
from plugins.misc import swap

DATA = b"\x41 "

aes = AES256CBC("asdfawefa")


ENCODER = [
    yenc
]


z = Engine(ENCODER)
x = z.encode(DATA)
y = z.decode(x)
print(y.decode())
assert (DATA == y)

