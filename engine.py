class Engine(object):
    def __init__(self, transforms):
        self._transforms = transforms

    @property
    def transforms(self):
        return self._transforms

    def _encode(self, func, x):
        return func.encode(x)

    def _decode(self, func, x):
        return func.decode(x)

    def encode(self, x):
        print(f"Encode:\t{x}")
        for func in self.transforms:
            x = self._encode(func, x)
            print(f"Encode:\t{func.__name__}\t:\t{x}")
        return x

    def decode(self, x):
        print(f"Decode:\t{x}")
        for func in self.transforms[::-1]:
            x = self._decode(func, x)
            print(f"Decode:\t{func.__name__}\t:\t{x}")
        return x